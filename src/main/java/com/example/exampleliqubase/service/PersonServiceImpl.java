package com.example.exampleliqubase.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import com.example.exampleliqubase.api.PersonService;
import com.example.exampleliqubase.dao.PersonMapper;
import com.example.exampleliqubase.dto.PersonDTO;
import com.example.exampleliqubase.model.Person;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private DataSource dataSource;

    @PostConstruct
    public void init() {
        System.out.println();
    }

    @Override
    @Transactional
    public PersonDTO save(PersonDTO personDTO) {

        Person person = modelMapper.map(personDTO, Person.class);
        //тут какая-то логика
        personMapper.save(person);

        //тут еще какая-то логика

        return modelMapper.map(person, PersonDTO.class);
    }

    @Override
    public PersonDTO getPerson() {
        return personMapper.getPerson();
    }
}
