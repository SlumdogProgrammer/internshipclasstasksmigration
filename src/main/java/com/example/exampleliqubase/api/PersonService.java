package com.example.exampleliqubase.api;

import java.util.List;

import com.example.exampleliqubase.dto.PersonDTO;

public interface PersonService {
    PersonDTO save(PersonDTO personDTO);

    PersonDTO getPerson();
}
