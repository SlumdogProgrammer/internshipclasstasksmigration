package com.example.exampleliqubase.api;

import com.example.exampleliqubase.dto.EmploymentDTO;

public interface EmploymentService {
    EmploymentDTO getEmployment();
    EmploymentDTO save(EmploymentDTO employmentDTO);
}
