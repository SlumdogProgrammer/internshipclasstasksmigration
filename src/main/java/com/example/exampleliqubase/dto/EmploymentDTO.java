package com.example.exampleliqubase.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmploymentDTO {
    private Long employment_id;
    private int version;
    private LocalDate start_dt;
    private LocalDate end_dt;
    private int work_type_id;
    private String organization_name;
    private String organization_address;
    private String position_name;
    private Long person_id;
}
