package com.example.exampleliqubase.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Person {
    private Long person_id;
    private String first_name;
    private String last_name;
    private String middle_name;
    private LocalDate birth_date;
    private String gender;
}
