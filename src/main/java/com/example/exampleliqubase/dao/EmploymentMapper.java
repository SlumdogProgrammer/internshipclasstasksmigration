package com.example.exampleliqubase.dao;

import com.example.exampleliqubase.dto.EmploymentDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface EmploymentMapper {
    @Select("select * from employment")
    List<EmploymentDTO> getEmployments();
}
