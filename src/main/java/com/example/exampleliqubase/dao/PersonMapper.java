package com.example.exampleliqubase.dao;

import java.util.List;

import com.example.exampleliqubase.dto.PersonDTO;
import com.example.exampleliqubase.model.Person;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface PersonMapper {

    /*@SelectKey(resultType = Long.class, keyProperty = "userId", before = true,
            statement = "select nextval('account_entity_liquibase_seq')")
    @Insert("insert into account_entity_liquibase (user_id, username, password, country) " +
            "values (#{userId}, #{username} ,#{password} ,#{country} )")
    void save(Person person);

    @Select("select * from account_entity_liquibase")
    List<Person> getListAccounts();*/

    @Insert("insert into person " +
            "values (#{person_id}, #{first_name} ,#{last_name} ,#{middle_name}, #{birth_date},#{gender})")
    void save(Person person);

    @Select("select * from person where person_id=#{id}")
    PersonDTO getPerson();
}
